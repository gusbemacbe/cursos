﻿// 🇬🇧 Declaring some basic value type variables
// 🇵🇹 A declarar algumas variáveis básicas de tipo de valor
// 🇮🇹 Dichiarazione di alcune variabili di tipo di valore di base  
// 🇪🇸 Declaración de algunas variables básicas de tipo de valor
// 🇫🇷 Déclaration de quelques variables de type de valeur de base
int i = 10;
float f = 20.5f;
decimal d = 100.5m;
bool b = true;
char c = 'c';

// 🇬🇧 Declaring a string – it is a collection of characters
// 🇵🇹 A declarar uma string - é uma colecção de caracteres
// 🇮🇹 Dichiarazione di una stringa - è una collezione di caratteri
// 🇪🇸 Declaración de un string - es una colección de caracteres
// 🇫🇷 Déclaration d'une chaîne - c'est une collection de caractères  
string str = "a string";

// 🇬🇧 Declaring an implicit variable
// 🇵🇹 A declarar uma variável implícita
// 🇮🇹 Dichiarazione di una variabile implicita
// 🇪🇸 Declaración de una variable implícita
// 🇫🇷 Déclaration d‘une variable implicite
var x = 10;
var z = "Hello!";

// 🇬🇧 Declaring an array of values
// 🇵🇹 A declarar uma matriz de valores
// 🇮🇹 Dichiarazione di una matrice di valori
// 🇪🇸 Declaración de una matriz de valores
// 🇫🇷 Déclaration d'une tableau de valeurs
int[] vals = new int[5];
string[] strs = { "one", "two", "three" };

// 🇬🇧 Printing the values using a formatting string
// 🇵🇹 Imprimir os valores ao utilizar uma string de formatação
// 🇮🇹 Stampare i valori usando una stringa di formattazione
// 🇪🇸 Impresión de los valores usando una cadena de formato
// 🇫🇷 Imprimer les valeurs en utilisant une chaîne de formatage
Console.WriteLine("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}", i, c, b, str, f, d, x, z);

// 🇬🇧 The value «null» means «no value»
// 🇵🇹 O valor «null» significa «nenhum valor»
// 🇮🇹 Il valore «null» significa «nessun valore»
// 🇪🇸 El valor «null» significa «ningún valor»
// 🇫🇷 La valeur «null» signifie «aucune valeur»
// object obj = null;
// Console.WriteLine(obj);

// 🇬🇧 Implicit conversion between types
// 🇵🇹 Conversão implícita entre tipos
// 🇮🇹 Conversione implicita tra tipi
// 🇪🇸 Conversión implícita entre tipos
// 🇫🇷 Conversion implicite entre types
long bignum;
bignum = i;

// 🇬🇧 Explicit conversions
// 🇵🇹 Conversões explícitas
// 🇮🇹 Conversioni esplicite
// 🇪🇸 Conversiónes explícitas
// 🇫🇷 Conversions explicites
float int_as_float = (float)i;
Console.WriteLine($"Converting a number to a float: {int_as_float}");

int float_as_int = (int)f;
Console.WriteLine($"Converting a float to an int: {float_as_int}");